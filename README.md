# Taps Aff
Use the [metaweather API](https://www.metaweather.com/api/) to clone
[www.taps-aff.co.uk](http://taps-aff.co.uk/).  Allow a user to search 
for a location then return whether the weather is "Taps Aff" or not.