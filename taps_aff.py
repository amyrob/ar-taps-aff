
import requests

search_url = "https://www.metaweather.com/api/location/search/"
api_url = "https://www.metaweather.com/api/location/"


def get_woeid(location):
    loc = dict(query=str(location))
    response = requests.get(search_url, loc)

    if (response.ok):
        location_info = response.json()
        woeid = location_info[0]["woeid"]
        return (str(woeid))
    else:
        raise ValueError('Request failed')


def get_temperature_info(location):
    lowloc = str.lower(location)
    woeid = get_woeid(lowloc)
    response = requests.get(api_url+woeid)
    
    if (response.ok):
        info = response.json()
        for n in info["consolidated_weather"]:
            temp = "{:.0f}".format(n["the_temp"])
            return (int(temp))
    else:
        raise ValueError("Request not OK")


def taps_aff(location):
    temp = get_temperature_info(location)
    if (temp >= 21):
        print ("OOOH...it's "+str(temp)+"°C that means it's TAP'S AFF WEATHER 😎")
    else:
        print ("Sorry pal, "+str(temp)+"°C means yer tap's staying oan the day 😩")




if __name__ == '__main__':
    city = str(input("Whit city are ye lookin' fur?"))
    taps_aff(city)
